# -*- coding: UTF-8 -*-

# This file is a part of fntom which is a Python3 package that implements a
# finite, negative, totally ordered monoid together with methods to compute its
# one-element Rees co-extensions.
#
# Copyright (C) 2021 Milan Petrík <milan.petrik@protonmail.com>
#
# Web page of the program: <https://gitlab.com/petrikm/fntom>
#
# fntom is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# fntom is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# fntom. If not, see <https://www.gnu.org/licenses/>.

"""
    This script serves to generate f. n. tomonoids as co-extensions starting
    from the trivial monoid.

    It is supposed to be running for a long time on a single computer in a
    single thread.

    The script generates and saves to files f. n. tomonoids that are:

      * general,
      * Archimedean,
      * commutative,
      * commutative and Archimedean.

    These four tasks are performed sequentially: the program switches between
    them in a given period (which is 1 hour).

    The generated f. n. tomonoids are written to files in the working directory
    with names "fntom_....txt" where "..." stands for "gen", "a", "c", or "ca".

    Furthermore, short reports with numbers of generated f. n. tomonoids are
    written to files "report_....txt" to the directory given by `reportDir`.

    The script continues generating until the size of every output file exceeds
    the limit given by `maxFileSize`.
    The script can be cancelled anytime by pressing Ctrl-C.
"""

import os
import sys
import fntom
import timeit
import datetime

timePeriod = 3600 # in seconds
reportDir = "./report/"
maxFileSize = 4 * 1024**3 # in bytes

stepCounter = 0 

options = [
        { "name": "gen", "archimedean": False, "commutative": False },
        { "name": "c",   "archimedean": False, "commutative": True },
        { "name": "a",   "archimedean": True,  "commutative": False },
        { "name": "ca",  "archimedean": True,  "commutative": True }
    ]

finish = False
while not finish:
    stepCounter += 1
    finish = True
    for opt in options:
        fileName = "fntom_" + opt["name"] + ".txt"
        try:
            fileSize = os.path.getsize(fileName)
        except FileNotFoundError:
            print("Creating", fileName)
            fntomonoids, report = fntom.generateFNTomonoids(
                    upToSize = 5,
                    commutative = opt["commutative"],
                    archimedean = opt["archimedean"],
                    displayProgress = True,
                    counter = fntom.Counter())
            fntom.saveToCompressed(
                    fileName,
                    fntomonoids,
                    displayProgress = True,
                    report = report)
            fileSize = os.path.getsize(fileName)
        if fileSize < maxFileSize:
            finish = False
            try:
                f = fntom.CompressedFile(path = fileName)
            except FileNotFoundError:
                print("ERROR: FAILED TO OPEN", fileName)
                continue
            print("***", fileName, "size:", fileSize, "***")
            f.save("bak." + fileName, displayProgress = True, includeReport = False)
            timeStart = timeit.default_timer()
            counter = fntom.Counter(number = f.getLastIdentifier())
            tomIndex = 0
            while tomIndex < len(f.lines):
                tom = f.getFNTOMonoid(tomIndex = tomIndex)
                # time report:
                for i in range(80):
                    print(end = "\b")
                timeCurrent = timeit.default_timer()
                timeRemaining = timePeriod - (timeCurrent - timeStart)
                if timeRemaining < 0:
                    print()
                    break
                print(opt["name"], str(tom.size), datetime.timedelta(seconds=timeRemaining), end = "", flush = "")
                if not tom.closed:
                    coExtensions = tom.computeCoExtensions(
                            counter,
                            commutative = opt["commutative"],
                            archimedean = opt["archimedean"])
                    for coext in coExtensions:
                        f.lines.append(coext.exportToCompressedText() + "\n")
                    tom.closed = True
                    f.lines[tomIndex] = tom.exportToCompressedText() + "\n"
                tomIndex += 1
            f.save(fileName, displayProgress = True, includeReport = True)
            reportPath = reportDir + "report_" + opt["name"] + "_" + f"{stepCounter:05d}" + ".txt"
            with open(reportPath, "w") as reportFile:
                reportFile.write(f.exportReportToText(f.report))
