# -*- coding: UTF-8 -*-

# This file is a part of fntom which is a Python3 package that implements a
# finite, negative, totally ordered monoid together with methods to compute its
# one-element Rees co-extensions.
#
# Copyright (C) 2021 Milan Petrík <milan.petrik@protonmail.com>
#
# Web page of the program: <https://gitlab.com/petrikm/fntom>
#
# fntom is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# fntom is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# fntom. If not, see <https://www.gnu.org/licenses/>.

"""
Implementation of unit tests for the package fntom.

To be used with the python module
    [unittest](https://docs.python.org/3/library/unittest.html).

To run the test, navigate to the root directory of the project and type:

    python3 -m unittest discover -s tests

or:
    
    python3 -m nose2
"""

import unittest
import random
import fntom

class TestCoextensions(unittest.TestCase):

    def testCounter(self):
        # Tests whether the next value given by Counter is greater by 1.
        counter = fntom.Counter()
        value = counter.getNew()
        for i in range(10):
            current = counter.getCurrent()
            increment = random.randint(1,100)
            for i in range(increment):
                value = counter.getNew()
            self.assertEqual(value, current + increment)

    def testBase62(self):
        # Tests conversions between decimal numbers and Base62 numbers.
        self.assertEqual(fntom.convertBase62ToDecimal("10"), 62)
        self.assertEqual(fntom.convertDecimalToBase62(62), "10")
        self.assertEqual(fntom.convertBase62ToDecimal("A0"), 620)
        self.assertEqual(fntom.convertDecimalToBase62(620), "A0")
        for i in range(100):
            value = random.randint(1000, 1000000000)
            base62 = fntom.convertDecimalToBase62(value)
            decimal = fntom.convertBase62ToDecimal(base62)
            base62Again = fntom.convertDecimalToBase62(decimal)
            self.assertEqual(value, decimal)
            self.assertEqual(base62, base62Again)

    def test0xyz1(self):
        # Tests conversions between decimal numbers and the "0xyz1" system.
        self.assertEqual(fntom.convert0xyz1ToDecimal("0", 26), 25)
        self.assertEqual(fntom.convert0xyz1ToDecimal("x", 26), 3)
        self.assertEqual(fntom.convert0xyz1ToDecimal("1", 26), 0)
        self.assertEqual(fntom.convertDecimalTo0xyz1(25, 26), "0")
        self.assertEqual(fntom.convertDecimalTo0xyz1(3, 26), "x")
        self.assertEqual(fntom.convertDecimalTo0xyz1(0, 26), "1")
        for i in range(100):
            size = random.randint(2, 26)
            value = random.randint(0, size - 1)
            element = fntom.convertDecimalTo0xyz1(value, size)
            decimal = fntom.convert0xyz1ToDecimal(element, size)
            elementAgain = fntom.convertDecimalTo0xyz1(decimal, size)
            self.assertEqual(value, decimal)
            self.assertEqual(element, elementAgain)

    def convertTextTableToListTable(self, strTable):
        # Auxiliary method for `testLevelEquivalence`.
        # Converts f. n. tomonoid table given by string from method
        # `LevelEquivalence.exportTableToText` to a table given as a list of
        # lists which can be further compared with the table given by the
        # method `LevelEquivalence.getTable`.
        result = []
        for row in strTable.split("\n"):
            result.append(row.replace(" ", "").split(","))
        return result

    def testLevelEquivalence(self):
        # Tests loading and exporting Cayley tables that define f. n.
        # tomonoids.
        # The following three tables decribes the same non-commutative f. n.
        # tomonoid.
        # Tomonoid table with Base62 values:
        initialBase62Table = [
            ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A"],
            ["1", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A"],
            ["2", "2", "2", "3", "4", "5", "6", "8", "8", "9", "A"],
            ["3", "3", "3", "3", "4", "5", "6", "8", "8", "9", "A"],
            ["4", "4", "4", "4", "4", "5", "6", "9", "9", "9", "A"],
            ["5", "5", "5", "5", "5", "5", "6", "9", "9", "9", "A"],
            ["6", "6", "6", "6", "6", "6", "6", "9", "9", "9", "A"],
            ["7", "7", "7", "7", "8", "8", "A", "A", "A", "A", "A"],
            ["8", "8", "8", "8", "8", "8", "A", "A", "A", "A", "A"],
            ["9", "9", "9", "9", "9", "9", "A", "A", "A", "A", "A"],
            ["A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A"]]
        # Tomonoid table with integer values:
        initialIntTable = [
            [ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10],
            [ 1,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10],
            [ 2,  2,  2,  3,  4,  5,  6,  8,  8,  9, 10],
            [ 3,  3,  3,  3,  4,  5,  6,  8,  8,  9, 10],
            [ 4,  4,  4,  4,  4,  5,  6,  9,  9,  9, 10],
            [ 5,  5,  5,  5,  5,  5,  6,  9,  9,  9, 10],
            [ 6,  6,  6,  6,  6,  6,  6,  9,  9,  9, 10],
            [ 7,  7,  7,  7,  8,  8, 10, 10, 10, 10, 10],
            [ 8,  8,  8,  8,  8,  8, 10, 10, 10, 10, 10],
            [ 9,  9,  9,  9,  9,  9, 10, 10, 10, 10, 10],
            [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]]
        # Tomonoid table with left-right flipped "0xyz1" values:
        initialFntomTable = [
            ["0", "r", "s", "t", "u", "v", "w", "x", "y", "z", "1"],
            ["0", "r", "s", "t", "u", "v", "w", "x", "y", "z", "z"],
            ["0", "r", "s", "s", "u", "v", "w", "x", "y", "y", "y"],
            ["0", "r", "s", "s", "u", "v", "w", "x", "x", "x", "x"],
            ["0", "r", "r", "r", "u", "v", "w", "w", "w", "w", "w"],
            ["0", "r", "r", "r", "u", "v", "v", "v", "v", "v", "v"],
            ["0", "r", "r", "r", "u", "u", "u", "u", "u", "u", "u"],
            ["0", "0", "0", "0", "0", "s", "s", "t", "t", "t", "t"],
            ["0", "0", "0", "0", "0", "s", "s", "s", "s", "s", "s"],
            ["0", "0", "0", "0", "0", "r", "r", "r", "r", "r", "r"],
            ["0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"]]
        # From each table an instance of LevelEquivalence is created:
        levEq = 3 * [None]
        levEq[0] = fntom.LevelEquivalence(base62Table = initialBase62Table)
        levEq[1] = fntom.LevelEquivalence(intTable = initialIntTable)
        levEq[2] = fntom.LevelEquivalence(xyzTable = initialFntomTable)
        # Test whether the integer tables given by `getTable` are mutually
        # equal and whether they are all equal to the initial integer table:
        for i in range(len(levEq)):
            #print(i, levEq[i].getTable() == initialIntTable)
            self.assertEqual(levEq[i].getTable(), initialIntTable)
            for j in range(i + 1, len(levEq)):
                #print(i, j, levEq[i].getTable() == levEq[j].getTable())
                self.assertEqual(levEq[i].getTable(), levEq[j].getTable())
        # Test whether the tables given by `exportTableToText` are equal to the
        # initial tables:
        for k in range(len(levEq)):
            textTable = levEq[k].exportTableToText(tableSymbols = "base62")
            listTable = self.convertTextTableToListTable(textTable)
            self.assertEqual(listTable, initialBase62Table)
            textTable = levEq[k].exportTableToText(tableSymbols = "int")
            listTable = self.convertTextTableToListTable(textTable)
            for i in range(len(listTable)):
                for j in range(len(listTable[i])):
                    listTable[i][j] = int(listTable[i][j])
            self.assertEqual(listTable, initialIntTable)
            textTable = levEq[k].exportTableToText(tableSymbols = "0xyz1")
            listTable = self.convertTextTableToListTable(textTable)
            self.assertEqual(listTable, initialFntomTable)

    def testFNTOMonoidsGeneral(self):
        # Generates all general f. n. tomonoids from the trivial monoid up to
        # the monoids of a given size and tests whether their numbers are as
        # expected.
        depth = 6
        expectedNumbers = [0, 1, 1, 2, 8, 44, 308, 2641, 27120, 332507, 5035455]
        expectedNumbers = expectedNumbers[:depth + 2]
        monoids, report = fntom.generateFNTomonoids(
                method = "levelset",
                startingFNTOMonoid = None,
                depth = depth,
                archimedean = False,
                commutative = False,
                counter = None)
        numbers = (depth + 2) * [0]
        for monoid in monoids:
            numbers[monoid.size] += 1
        self.assertEqual(numbers, expectedNumbers)
        reportedNumbers = [ 0 ]
        for i in range(1, len(report)):
            reportedNumbers.append(report[i]["number"])
        self.assertEqual(reportedNumbers, expectedNumbers)

    def testFNTOMonoidsArchimedean(self):
        # Generates all Archimedean f. n. tomonoids from the trivial monoid up to
        # the monoids of a given size and tests whether their numbers are as
        # expected.
        depth = 6
        expectedNumbers = [0, 1, 1, 1, 2, 8, 44, 333, 3543, 54954, 1297705]
        expectedNumbers = expectedNumbers[:depth + 2]
        monoids, report = fntom.generateFNTomonoids(
                method = "levelset",
                startingFNTOMonoid = None,
                depth = depth,
                archimedean = True,
                commutative = False,
                counter = None)
        numbers = (depth + 2) * [0]
        for monoid in monoids:
            numbers[monoid.size] += 1
        self.assertEqual(numbers, expectedNumbers)
        reportedNumbers = [ 0 ]
        for i in range(1, len(report)):
            reportedNumbers.append(report[i]["number"])
        self.assertEqual(reportedNumbers, expectedNumbers)

    def testFNTOMonoidsCommutative(self):
        # Generates all commutative f. n. tomonoids from the trivial monoid up
        # to the monoids of a given size and tests whether their numbers are as
        # expected.
        depth = 6
        expectedNumbers = [0, 1, 1, 2, 6, 22, 94, 451, 2386, 13775, 86417]
        expectedNumbers = expectedNumbers[:depth + 2]
        monoids, report = fntom.generateFNTomonoids(
                method = "levelset",
                startingFNTOMonoid = None,
                depth = depth,
                archimedean = False,
                commutative = True,
                counter = None)
        numbers = (depth + 2) * [0]
        for monoid in monoids:
            numbers[monoid.size] += 1
        self.assertEqual(numbers, expectedNumbers)
        reportedNumbers = [ 0 ]
        for i in range(1, len(report)):
            reportedNumbers.append(report[i]["number"])
        self.assertEqual(reportedNumbers, expectedNumbers)

    def testFNTOMonoidsCommutativeArchimedean(self):
        # Generates all commutative Archimedean f. n. tomonoids from the
        # trivial monoid up to the monoids of a given size and tests whether
        # their numbers are as expected.
        depth = 7
        expectedNumbers = [0, 1, 1, 1, 2, 6, 22, 95, 471, 2670, 17387]
        expectedNumbers = expectedNumbers[:depth + 2]
        monoids, report = fntom.generateFNTomonoids(
                method = "levelset",
                startingFNTOMonoid = None,
                depth = depth,
                archimedean = True,
                commutative = True,
                counter = None)
        numbers = (depth + 2) * [0]
        for monoid in monoids:
            numbers[monoid.size] += 1
        self.assertEqual(numbers, expectedNumbers)
        reportedNumbers = [ 0 ]
        for i in range(1, len(report)):
            reportedNumbers.append(report[i]["number"])
        self.assertEqual(reportedNumbers, expectedNumbers)

if __name__ == '__main__':
    unittest.main()

