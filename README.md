fntom
=====

This package contains an implementation of the algorithm that serves to find
all the one-element Rees co-extensions of a given finite, negative totally
ordered monoid (abbreviated by f. n. tomonoid).

A commutative f. n. tomonoid represents a conjunction in the semantics of a
many-valued (fuzzy) logic that has finitely many totally ordered logical
values.


Installation
------------

Install the package from [PyPI](https://pypi.org/) utilizing the
[pip](https://pypi.org/project/pip/) module:

    python -m pip install fntom

Alternatively, you can download and unpack this package and install it by:

    python -m pip install path

where `path` refers to the directory where `setup.py` is present.


A finite set of logical values
------------------------------

An example of a totally ordered finite set of (five) logical values is:

    0, 1/4, 2/4, 3/4, 1

where `1` represents the "truth", `0` represents the "falsity", and the rest of
the values represent various degrees of "partial truth".

In the papers (see **References** below), that are the foundation of this
program, such a set would be described as

    0, x, y, z, 1

since letters, compared to fractions, are easier to write and read.


Many-valued conjunctions
------------------------

A conjunction on a totally ordered set of logical values is every binary
operation * such that:

  - `*` is associative, i.e, `(x*y)*y = x*(y*z)` for every `x`, `y`, and `z`
    from the set,
  - `*` is commutative , i.e, `x*y = y*x` for every `x` and `y` from the set,
  - `*` is monotone, i.e, `x <= y` implies `x*z <= y*z` for every `x`, `y`, and
    `z` from the set,
  - `1` is the neutral (unit) element of *, i.e, `x*1 = 1*x = x` for every `x`
    from the set.

Hence the operation of the conjunction is not unique like in the case of the
classical logic with two truth values.
For example, in the case of four logical values there are six distinct
conjunctions given by the following Cayley tables:

| 0 | y | z | 1 |
| - | - | - | - |
| 0 | 0 | 0 | z |
| 0 | 0 | 0 | y |
| 0 | 0 | 0 | 1 |

| 0 | y | z | 1 |
| - | - | - | - |
| 0 | 0 | y | z |
| 0 | 0 | 0 | y |
| 0 | 0 | 0 | 1 |

| 0 | y | z | 1 |
| - | - | - | - |
| 0 | y | y | z |
| 0 | y | y | y |
| 0 | 0 | 0 | 1 |

| 0 | y | z | 1 |
| - | - | - | - |
| 0 | 0 | z | z |
| 0 | 0 | 0 | y |
| 0 | 0 | 0 | 1 |

| 0 | y | z | 1 |
| - | - | - | - |
| 0 | y | z | z |
| 0 | 0 | y | y |
| 0 | 0 | 0 | 1 |

| 0 | y | z | 1 |
| - | - | - | - |
| 0 | y | z | z |
| 0 | y | y | y |
| 0 | 0 | 0 | 1 |

Note that these Cayley tables are modified: they are left-right flipped.
This way of depicting conjunctions is utilized in the referenced papers since
it corresponds with the way of depicting *triangular norms* which represents
many-valued conjunctions defined on the real unit interval `[0,1]`.


Finite, negative, totally ordered monoids (f. n. tomonoids)
-----------------------------------------------------------

A _finite, negative, totally ordered monoid_ (abbreviated as _f. n. tomonoid_)
of a size `n` (where `n` is a natural number) is a set `S` of `n` elements,
denoted and ordered as:

    0 < ... < x < y < z < 1

Hence, `0` is the bottom element and `1` is the top element.
Furthermore, this set is endowed with a binary operation `*` that satisfies,
for every `a`, `b`, and `c` from the set `S`:

  - `(a*b)*b = a*(b*c)` (associativity),
  - if `a <= b` then `a*c <= b*c` (monotonicity, or also, compatibility with
    the total order),
  - `a*1 = 1*a = a` (the top element `1` is also the neutral (unit) element of
    `*`).

An element `a` of a f. n. tomonoid is called _idempotent_ if `a*a=a`.
The elements `0` and `1` are always idempotent and the are called the _trivial
idempotents_.

A f. n. tomonoid is called:

  - _commutative_ if `a*b=b*a` for every `a` and `b` from the set `S`,
  - _Archimedean_ if `0` and `1` are the only idempotents.

Clearly, a (commutative) f. n. tomonoid represents a totally ordered set of
truth values together with a conjunction which is represented by the monoidal
binary operation.


One-element Rees quotients
--------------------------

Given a f. n. tomonoid `S`, its one-element Rees quotient can be constructed by
"merging" its lowest element `0` and its second lowest element (we denote it by
&alpha; and we call it the _atom_) into a new bottom element.
For example, the one-element Rees quotient of the f. n. tomonoid:

| 0 | u | v | w | x | y | z | 1 |
| - | - | - | - | - | - | - | - |
| 0 | 0 | v | w | x | y | z | z |
| 0 | 0 | v | w | w | y | y | y |
| 0 | 0 | 0 | 0 | 0 | v | x | x |
| 0 | 0 | 0 | 0 | 0 | v | w | w |
| 0 | 0 | 0 | 0 | 0 | v | v | v |
| 0 | 0 | 0 | 0 | 0 | 0 | 0 | u |
| 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |

is the f. n. tomonoid:

| 0 | v | w | x | y | z | 1 |
| - | - | - | - | - | - | - |
| 0 | v | w | x | y | z | z |
| 0 | v | w | w | y | y | y |
| 0 | 0 | 0 | 0 | v | x | x |
| 0 | 0 | 0 | 0 | v | w | w |
| 0 | 0 | 0 | 0 | v | v | v |
| 0 | 0 | 0 | 0 | 0 | 0 | 0 |

Further, the one-element Rees quotient of the latter f. n. tomonoid is the f.
n. tomonoid:

| 0 | w | x | y | z | 1 |
| - | - | - | - | - | - |
| 0 | w | x | y | z | z |
| 0 | w | w | y | y | y |
| 0 | 0 | 0 | 0 | x | x |
| 0 | 0 | 0 | 0 | w | w |
| 0 | 0 | 0 | 0 | 0 | 0 |

and so on.
This way we eventually end up with the trivial monoid which is a set that
consists of the unit element, only.

Instead of one-element Rees quotient we say just _one-element quotient_.


One-element Rees co-extensions
------------------------------

If a f. n. tomonoid `T` is the one-element Rees quotient of a f. n. tomonoid
`S` then `S` is a _one-element Rees co-extension_ of `T`.
Hence, in the previous section, the first f. n. tomonoid is a one-element Rees
co-extension of the second one and the second f. n. tomonoid is a one-element
Rees co-extension of the third one.

Instead of one-element Rees co-extension we say just _one-element
co-extension_, or just _co-extension_.

Note that, while one-element quotients are unique, one-element co-extensions
are not.
For example, the f. n. tomonoid:

| 0 | x | y | z | 1 |
| - | - | - | - | - |
| 0 | 0 | 0 | x | z |
| 0 | 0 | 0 | 0 | y |
| 0 | 0 | 0 | 0 | x |
| 0 | 0 | 0 | 0 | 0 |

has 8 co-extensions:

| 0 | w | x | y | z | 1 |
| - | - | - | - | - | - |
| 0 | 0 | 0 | 0 | x | z |
| 0 | 0 | 0 | 0 | 0 | y |
| 0 | 0 | 0 | 0 | 0 | x |
| 0 | 0 | 0 | 0 | 0 | w |
| 0 | 0 | 0 | 0 | 0 | 0 |

| 0 | w | x | y | z | 1 |
| - | - | - | - | - | - |
| 0 | 0 | 0 | 0 | x | z |
| 0 | 0 | 0 | 0 | w | y |
| 0 | 0 | 0 | 0 | 0 | x |
| 0 | 0 | 0 | 0 | 0 | w |
| 0 | 0 | 0 | 0 | 0 | 0 |

| 0 | w | x | y | z | 1 |
| - | - | - | - | - | - |
| 0 | 0 | 0 | w | x | z |
| 0 | 0 | 0 | 0 | 0 | y |
| 0 | 0 | 0 | 0 | 0 | x |
| 0 | 0 | 0 | 0 | 0 | w |
| 0 | 0 | 0 | 0 | 0 | 0 |

| 0 | w | x | y | z | 1 |
| - | - | - | - | - | - |
| 0 | 0 | 0 | w | x | z |
| 0 | 0 | 0 | 0 | w | y |
| 0 | 0 | 0 | 0 | 0 | x |
| 0 | 0 | 0 | 0 | 0 | w |
| 0 | 0 | 0 | 0 | 0 | 0 |

| 0 | w | x | y | z | 1 |
| - | - | - | - | - | - |
| 0 | 0 | 0 | w | x | z |
| 0 | 0 | 0 | w | w | y |
| 0 | 0 | 0 | 0 | 0 | x |
| 0 | 0 | 0 | 0 | 0 | w |
| 0 | 0 | 0 | 0 | 0 | 0 |

| 0 | w | x | y | z | 1 |
| - | - | - | - | - | - |
| 0 | 0 | w | w | x | z |
| 0 | 0 | 0 | 0 | w | y |
| 0 | 0 | 0 | 0 | w | x |
| 0 | 0 | 0 | 0 | 0 | w |
| 0 | 0 | 0 | 0 | 0 | 0 |

| 0 | w | x | y | z | 1 |
| - | - | - | - | - | - |
| 0 | 0 | w | w | x | z |
| 0 | 0 | 0 | w | w | y |
| 0 | 0 | 0 | 0 | w | x |
| 0 | 0 | 0 | 0 | 0 | w |
| 0 | 0 | 0 | 0 | 0 | 0 |

| 0 | w | x | y | z | 1 |
| - | - | - | - | - | - |
| 0 | w | w | w | x | z |
| 0 | w | w | w | w | y |
| 0 | w | w | w | w | x |
| 0 | w | w | w | w | w |
| 0 | 0 | 0 | 0 | 0 | 0 |

Notice that the second and the third one are non-commutative while the rest is
commutative.


Example
-------

The following program defines a f. n. tomonoid with the Cayley table:

| 0 | x | y | z | 1 |
| - | - | - | - | - |
| 0 | 0 | 0 | x | z |
| 0 | 0 | 0 | 0 | y |
| 0 | 0 | 0 | 0 | x |
| 0 | 0 | 0 | 0 | 0 |

Subsequently, it finds all its commutative one-element Rees co-extensions and
displays both the f. n. tomonoid and its co-extensions on the terminal output:

```python
    import fntom
    counter = fntom.Counter()
    xyzTable = [['0' ,'x' ,'y' ,'z' ,'1'],
                ['0' ,'0' ,'0' ,'x' ,'z'],
                ['0' ,'0' ,'0' ,'0' ,'y'],
                ['0' ,'0' ,'0' ,'0' ,'x'],
                ['0' ,'0' ,'0' ,'0' ,'0']]
    fntom = fntom.FNTOMonoid(counter.getNew(), xyzTable = xyzTable)
    coextensions = fntom.computeCoExtensions(counter, commutative = True)
    fntom.show()
    for coext in coextensions:
        coext.show()
```


Table with numbers of finite, negative tomonoids
------------------------------------------------

Running this program on a personal computer, the following numbers of f. n.
tomonoids have been achieved.

| size |    all, | Archimedean, | commutative, | Archimedean and commutative |
| ----:| -------:| ------------:| ------------:| ---------------------------:|
|    1 |       1 |            1 |            1 |                           1 |
|    2 |       1 |            1 |            1 |                           1 |
|    3 |       2 |            1 |            2 |                           1 |
|    4 |       8 |            2 |            6 |                           2 |
|    5 |      44 |            8 |           22 |                           6 |
|    6 |     308 |           44 |           94 |                          22 |
|    7 |    2641 |          333 |          451 |                          95 |
|    8 |   27120 |         3543 |         2386 |                         471 |
|    9 |  332507 |        54954 |        13775 |                        2670 |
|   10 | 5035455 |      1297705 |        86417 |                       17387 |


More documentation
------------------

For a detailed decription of the program and its purpose, see
the [documentation](docs/fntom/index.html)
generated by [pdoc3](https://pdoc3.github.io/pdoc/)
in the directory [docs/](docs/)
or read the papers listed in **References** below.


References
----------

For the theoretical description of the problem and for the description of the algorithm se the paper:

* M. Petrik and Th. Vetterlein.
    *Rees coextensions of finite, negative tomonoids.*
    Journal of Logic and Computation 27 (2017) 337-356.
    DOI: [10.1093/logcom/exv047](https://doi.org/10.1093/logcom/exv047),
    [PDF](papers/Petrik_Vetterlein__Coextensions__preprint.pdf).

For a more detailed description of the algorithm see the papers:

* M. Petrik and Th. Vetterlein.
    *Algorithm to generate finite negative totally ordered monoids.*
    In: IPMU 2016: 16th International Conference on Information Processing
        and Management of Uncertainty in Knowledge-Based Systems.
    Eindhoven, Netherlands, June 20-24, 2016.
    [PDF](papers/Petrik_Vetterlein__IPMU_2016__preprint.pdf).

* M. Petrik and Th. Vetterlein.
    *Algorithm to generate the Archimedean, finite, negative tomonoids.*
    In: Joint 7th International Conference on Soft Computing
        and Intelligent Systems and 15th International Symposium on Advanced Intelligent Systems.
    Kitakyushu, Japan, Dec. 3-6, 2014.
    DOI: [10.1109/SCIS-ISIS.2014.7044822](https://doi.org/10.1109/SCIS-ISIS.2014.7044822).
    [PDF](papers/Petrik_Vetterlein__SCIS_ISIS_2014__preprint.pdf).

For more details on one-element co-extensions of finite, negative, tomonoids see the paper:

* M. Petrik and Th. Vetterlein.
    *Rees coextensions of finite tomonoids and free pomonoids.*
    Semigroup Forum 99 (2019) 345-367.
    DOI: [10.1007/s00233-018-9972-z](https://doi.org/10.1007/s00233-018-9972-z),
    [PDF](papers/Petrik_Vetterlein__Pomonoids__preprint.pdf).

For an overview of everything above, see the habilitation thesis:

* M. Petrik.
    *Many-Valued Conjunctions.*
    Habilitation thesis,
    Czech Technical University in Prague,
    Faculty of Electrical Engineering,
    Prague, Czech Republic,
    2020.
    [PDF](papers/Petrik__Habilitation_Thesis.pdf).
    Available at
    [Czech Technical University Digital Library](https://dspace.cvut.cz/handle/10467/91539).

